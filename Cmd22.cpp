#include <iostream>
#include <cstdlib>
#include <cstdlib>
#include <linux/i2c-dev.h>
#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "Cmd22.h"
using namespace std ;


Cmd22::Cmd22(int fd, int address)
{
		m_fd = fd ;
		m_address = address;
		Init();
}

void Cmd22::Init()
{
	// Set the port options and set the address of the device we wish to speak to
	if (ioctl(m_fd, I2C_SLAVE, m_address) < 0) {					
		cout << "Cmd22:Unable to get bus access to talk to slave\n" ;
		exit(1);
	}
	
	// This is the register we wish to read software version from
	m_buf[0] = 7;													
	
	// Send regiter to read from
	if ((write(m_fd, m_buf, 1)) != 1) {								
		cout << "Cmd22:Error writing to i2c slave\n";
		exit(1);
	}
	
	// Read back data into buf[]
	if (read(m_fd, m_buf, 1) != 1) {								
		cout <<  m_buf << "\n";
		cout << "Cmd22:Unable to read from slave\n" ;
		exit(1);
	}
	else {
		cout << "Cmd22:Software v: " << m_buf[0] << "\n";
	}
	
}


void Cmd22::setAcceleration(void) 
{
	// Set the port options and set the address of the device we wish to speak to
	if (ioctl(m_fd, I2C_SLAVE, m_address) < 0) {					
		cout << "Cmd22:Unable to get bus access to talk to slave\n" ;
		exit(1);
	}
	// Register to set acceleration
	m_buf[0] = 3;
	// Acceleration value to set											
	m_buf[1] = 255;												
	
	if ((write(m_fd, m_buf, 2)) != 2) {	
		cout << "Cmd22: Error writing to i2c slave\n";
		exit(1);
	}
	
}

void Cmd22::setMotorSpeeds(unsigned char speed) 
{
	// Set the port options and set the address of the device we wish to speak to
	if (ioctl(m_fd, I2C_SLAVE, m_address) < 0) {					
		cout << "Cmd22:Unable to get bus access to talk to slave\n" ;
		exit(1);
	}
	// Register to set speed of motor 1
	m_buf[0] = 1;													
	m_buf[1] = speed;
	
	if ((write(m_fd, m_buf, 2)) != 2) {
		cout << "Cmd22:Error writing to speed motor 1 / i2c slave\n";
		exit(1);
	}
	
	// Register to set speed of motor 2
	m_buf[0] = 2;													
	m_buf[1] = speed;
	
	if ((write(m_fd, m_buf, 2)) != 2) {
		cout << "Cmd22:Error writing to speed motor 2 / i2c slave\n" ;
		exit(1);
	}
	cout << "fin moteur speed : " << (int)speed << "\n" ;
}

