#include <iostream>
#include <cstdlib>
#include <linux/i2c-dev.h>
#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <fstream>
#include <string>
#include "Csrf08.h"
using namespace std ;

Csrf08::~Csrf08()
{
}


Csrf08::Csrf08(int fd, int address) //
{
	m_fd = fd;
	m_address = address ;
}

unsigned int Csrf08::GetDistance()
{
	unsigned int result ;
	
	if (ioctl(m_fd, I2C_SLAVE, m_address) < 0) {	
		// Set the port options and set the address of the device we wish to speak to				
		cout << ("srf08: impossible d avoir acces au bus \n");
		exit(1);
	}
	
	// Commands for performing a ranging on the SRF08
	m_buf[0] = 0;													
	m_buf[1] = 	1;
	
	// Write commands to the i2c port
	/*if ((write(m_fd, m_buf, 2)) != 2) {								
		cout << "srf08: Erreur ecriture i2c esclave\n";
		exit(1);
	}*/
	
	try{ 
		((write(m_fd, m_buf, 2)));}
		catch (exception e) {
											
		cout << "srf08: Erreur ecriture i2c esclave\n";
		exit(1);
	}
	
	// this sleep waits for the ping to come back
	usleep(80000);												
	
	// This is the register we wish to read from
	m_buf[0] = 0;													
	
	// Send register to read from
	if ((write(m_fd, m_buf, 1)) != 1) {								
		cout << "srf08:Error writing to i2c slave\n";
		exit(1);
	}
	
	// Read back data into buf[]
	if (read(m_fd, m_buf, 4) != 4) {								
		cout << "srf08:Unable to read from slave\n";
		cout << "srf08:Unable to read from slave\n";
		exit(1);
	}
	else {
		unsigned char highByte = m_buf[2];
		unsigned char lowByte = m_buf[3];
		result = (highByte <<8) + lowByte;			// Calculate range
		cout << "Logiciel v:" <<  (int)m_buf[0] << "\n" ;
		cout << "Lumiere: " << (int)m_buf[1] << "\n" ;
		cout << "Distance est: " << result << "\n" ;
	}
	
	return result ;
}
