class Cmd22 {
	public:
		Cmd22(int fd, int address);
		void setAcceleration(void);
		void setMotorSpeeds(unsigned char speed);
	private:
		void Init() ;
	private :
		int m_fd ;
		int m_address ;
		unsigned char m_buf[10];
	
} ;
